import numpy

def wofs_fusefunc(dest, src):
    """
    Fuse two WOfS water measurements represented as `ndarray`s.
    """
    # check for nodata flag
    destEmpty = (dest & 1).astype(numpy.bool)
    srcGood = (src == 128) | (src == 0)

    # both not empty, and dest not valid data
    both = ~destEmpty & ~((src & 1).astype(numpy.bool)) #& ~((dest == 128) | (dest == 0)))
    
    dest[destEmpty] = src[destEmpty]
    dest[both] |= src[both]
    
## ** NOTE this means that if only one says "good data" it will not be flagged as good
##    this is in order to be conservative on cloud shadow masking which can be incorrect around tile edges
    
