from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np
import copy
from itertools import groupby
from datacube.storage import masking
from datacube.testutils.io import rio_slurp_xarray
import datacube
dc = datacube.Datacube()

class le_level1(Transformation):
    '''
    combine all level 1 inputs: fc_veg, wofs_mask, saltpan_mask, mudflat_mask, DEM, mangrove
    '''

    def compute(self, data):
        year = str(data.time.data[0])
        data = data.squeeze(dim='time', drop=True)

        # generate DEM mask
        DEM = dc.load(product="ga_srtm_dem1sv1_0", like=data, measurements=["dem_h"]).squeeze(dim='time', drop=True)
        DEM_mask = np.where((DEM['dem_h'].values <= 6), 1, 0)

        # load mangroves
        mangroves = dc.load(product="mangrove_cover", like=data, measurements=["extent"], time=year)

        # calculate SI5, mudflat & saltpan masks
        si5 = (data['blue'] * data['red']) / (data['green'])
        mudflat_mask = np.where(si5 > 1000, 1, 0)
        mudflat_mask_nan = np.where((data['red'] == 0), 1, mudflat_mask)
        saltpan_mask = np.where(si5 < 1500, 1, 0)
        saltpan_mask_nan = np.where((data['red'] == 0), 1, saltpan_mask)

        # combine masks
        veg = ((data.fc_veg.data) * (1 - (data.wofs_mask.data)) * (saltpan_mask_nan) * (1-(mudflat_mask_nan * DEM_mask)))

        if mangroves:
            veg = np.where(mangroves.values == 1, 1, veg)

        vegetat_veg_cat = xr.DataArray(veg, coords=data.coords, dims=data.fc_veg.dims)
        return vegetat_veg_cat.to_dataset(name='vegetat_veg_cat')

    def measurements(self, input_measurements):
        return {'vegetat_veg_cat': Measurement(name='vegetat_veg_cat', dtype='float32', nodata=float('nan'), units='1')}
