"""
run as

python pop_l3_le_template.py -f ../vec_rois/level3_test_sites.gpkg -l level3_test_sites -t ./l3_le_template.yaml -o ./out_configs/ -c level3.sh -p ../imgouts/

"""

import argparse
import jinja2
import os.path

from osgeo import gdal

os.makedirs('./out_configs', exist_ok=True)
os.makedirs('../imgouts', exist_ok=True)

def get_feat_info(vec_file, vec_lyr, site_col='site', class_file_name_col='class_file_name', clr_file_name_col='clr_file_name'):
    """
A function which returns list of bboxs

* vecFile - vector file.
* vecLyr - layer within the vector file.
"""
    dsVecFile = gdal.OpenEx(vec_file, gdal.OF_UPDATE)
    if dsVecFile is None:
        raise Exception("Could not open '" + vec_file + "'")
        
    lyrVecObj = dsVecFile.GetLayerByName( vec_lyr )
    if lyrVecObj is None:
        raise Exception("Could not find layer '" + vec_lyr + "'")
        
    lyrDefn = lyrVecObj.GetLayerDefn()
    
    class_file_name_idx = -1
    clr_file_name_idx = -1
    site_idx = -1
    
    for i in range(lyrDefn.GetFieldCount()):
        if lyrDefn.GetFieldDefn(i).GetName() == class_file_name_col:
            class_file_name_idx = i
        elif lyrDefn.GetFieldDefn(i).GetName() == clr_file_name_col:
            clr_file_name_idx = i
        elif lyrDefn.GetFieldDefn(i).GetName() == site_col:
            site_idx = i
     
    if class_file_name_idx == -1:
        raise Exception('Cannot find class_file_name column in vector layer')
        
    if clr_file_name_idx == -1:
        raise Exception('Cannot find clr_file_name column in vector layer')

    if site_idx == -1:
        raise Exception('Cannot find site column in vector layer')
    
    outbboxs = []
    lyrVecObj.ResetReading()
    for feat in lyrVecObj:
        geom = feat.GetGeometryRef()
        if geom is not None:
            env = geom.GetEnvelope()
            bbox = dict()
            bbox['xmin'] = env[0]
            bbox['xmax'] = env[1]
            bbox['ymin'] = env[2]
            bbox['ymax'] = env[3]
            bbox['class_file_name'] = feat.GetFieldAsString(class_file_name_idx)
            bbox['clr_file_name'] = feat.GetFieldAsString(clr_file_name_idx)
            bbox['site'] = feat.GetFieldAsString(site_idx)
            outbboxs.append(bbox)
    dsVecFile = None
    return outbboxs


def writeList2File(dataList, outFile):
    """
    Write a list a text file, one line per item.
    """
    try:
        f = open(outFile, 'w')
        for item in dataList:
           f.write(str(item)+'\n')
        f.flush()
        f.close()
    except Exception as e:
        raise e


def produceConfigFiles(vec_file, vec_lyr, template_file, output_path, out_cmds_file, out_img_path=None):
    """
A function which takes in a vector file and template config file
and returns config files and a shell command to run the le_lccs classifcation
"""
    rois_info = get_feat_info(vec_file, vec_lyr)
    
    search_path, template_name = os.path.split(template_file)
    template_loader = jinja2.FileSystemLoader(searchpath=search_path)

    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template(template_name)
    
    cmd_base = 'python ../../livingearth_lccs/bin/le_lccs_odc.py'
    cmds = []
    i = 1
    for roi_info in rois_info:
        if out_img_path is not None:
            roi_info['class_file_name'] = os.path.join(out_img_path, roi_info['class_file_name'])
            roi_info['clr_file_name'] = os.path.join(out_img_path, roi_info['clr_file_name'])
            roi_info['site'] = os.path.join(output_path, roi_info['site'])

        output_text = template.render(roi_info)

        out_file_name = '{}{}'.format(roi_info['site'], ".yaml")

        with open(out_file_name, 'w') as out_file:
            out_file.write(output_text + '\n')
            out_file.flush()
            out_file.close()
        
        cmds.append('{} {}{}'.format(cmd_base, roi_info['site'], '.yaml'))
        i = i + 1
    writeList2File(cmds, out_cmds_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--vecfile", type=str, required=True, help="Specify vector file with extension.")
    parser.add_argument("-l", "--veclyr", type=str, required=True, help="Specify vector layer if different, otherwise specific vector file name.")
    parser.add_argument("-t", "--template", type=str, required=True, help="Specify the yaml template file.")
    parser.add_argument("-o", "--outpath", type=str, required=True, help="Output path name for output files.")
    parser.add_argument("-c", "--outcmds", type=str, required=True, help="Output shell commands file with extension.")
    parser.add_argument("-p", "--outimgpath", type=str, required=False, default=None, help="Optional output image file path.")


    args = parser.parse_args()

    
    produceConfigFiles(args.vecfile, args.veclyr, args.template, args.outpath, args.outcmds, args.outimgpath)

