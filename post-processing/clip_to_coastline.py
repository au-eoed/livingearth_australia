#!/usr/bin/env python3

import csv
import glob
import os
import subprocess

COASTAL_POLYGON = "/users/rsg/dac/scratch_network/dea_landcover/continental_run/aust_mainland_islands_dissolve.gpkg"
COASTAL_TILES_CSV = "au_tiles_coastal.csv"
OCEAN_TILES_CSV = "au_tiles_ocean.csv"
TILES_DIR = "/users/rsg/dac/scratch_network/dea_landcover/continental_run/{year}"

def read_tiles_list(coastal_tiles_csv=COASTAL_TILES_CSV):
    coastal_tile_identifiers = []
    with open(coastal_tiles_csv) as f:
        csv_file = csv.reader(f)
        for line in csv_file:
            tile_id = "_".join(line[1].split(","))
            coastal_tile_identifiers.append(tile_id)

    return coastal_tile_identifiers


def submit_clip_to_coastline_jobs(year, pattern, local=False):
    """
    Clip
    """
    tiles_dir = TILES_DIR.format(year=year)
    clipped_tiles_dir = os.path.join(tiles_dir, "clipped")

    os.makedirs(clipped_tiles_dir, exist_ok=True)

    grid_cmd = ["qsub",
                "-q", "lowpriority.q",
                "-P", "rsg",
                "-p", "-100",
                "-m", "n",
                "-b", "y",
                "-l", "throttle_dac=1",
                "-l", "mem_free=1G",
                "-e", clipped_tiles_dir,
                "-o", clipped_tiles_dir]


    # Get list of tiles on the coast
    coastal_tile_identifiers = read_tiles_list(COASTAL_TILES_CSV)
    ocean_tile_identifiers = read_tiles_list(OCEAN_TILES_CSV)

    # List all tiles
    tiles_list = glob.glob(os.path.join(tiles_dir, pattern))


    for tile in tiles_list:

        tile_id = "{}_{}".format(os.path.basename(tile).split("_")[1],
                                 os.path.basename(tile).split("_")[2].replace("L4","").replace("L3",""))

        if tile_id in coastal_tile_identifiers:
            out_tile = os.path.join(clipped_tiles_dir, os.path.basename(tile).replace(".tif", "_clipped.tif"))
            convert_cmd = ["gdalwarp", "-cutline", COASTAL_POLYGON,
                           "-of", "GTiff", "-co", "COMPRESS=LZW",
                           "-co", "TILED=YES", "-co", "BIGTIFF=YES",
                           tile, out_tile]
            if not os.path.isfile(out_tile):
                print(" ".join(convert_cmd))
                if "rgb" in pattern:
                    job_name = f"le_lccs-coast-clip-rgb-{year}-{tile_id}"
                else:
                    job_name = f"le_lccs-coast-clip-{year}-{tile_id}"

                submit_cmd = grid_cmd + ["-N", job_name] + convert_cmd
                if local:
                    subprocess.call(convert_cmd)
                else:
                    subprocess.call(submit_cmd)
        elif tile_id not in ocean_tile_identifiers:
            try:
                os.symlink(tile, os.path.join(clipped_tiles_dir, os.path.basename(tile)))
            except FileExistsError:
                pass


if __name__ == "__main__":

    run_locally = True
    for year in ["2010","2015"]:
        for pattern in ["*L4_v-1.0.0.tif","*L4_rgb_v-1.0.0.tif", "*L3_rgb_v-1.0.0.tif"]:
            submit_clip_to_coastline_jobs(year, pattern, local=run_locally)
