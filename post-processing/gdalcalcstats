#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Description: Optimises rasters for display by pre-calculating statistics and overviews.

# This file is part of 'gdalutils'
# Copyright (C) 2014 Sam Gillingham
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Original downloaded from https://bitbucket.org/chchrsc/lcrimageutils
#
# 2016/01/26 - Dan Clewley
#
# * Modified to use argparse and accept multiple images
# * Can use gdalinfo to calculate approximate stats (useful for hyperspectral
#   data with all bands)
# * Can use virtual filesystem to build for zip file (note this is quite slow).
import argparse
import os
import subprocess
import zipfile
from osgeo import gdal
gdal.UseExceptions()
from rios import calcstats
import get_gdal_drivers

def calc_approx_stats(image):
   """Add approximate stats to an image using gdalinfo"""
   subprocess.check_output(["gdalinfo", "-stats",
                            "-approx_stats", image])

if __name__ == "__main__":
   parser = argparse.ArgumentParser(description="Optimise rasters for display "
                                    "by calculating statistics and adding "
                                    "overviews.")
   parser.add_argument("infiles",nargs="+",
                       help="Input file(s)")
   parser.add_argument("--ignore", default=None,
                       type=float,
                       help="Value to ignore (default=None)")
   parser.add_argument("--approx_stats", default=False,
                       action="store_true",
                       help="Calculate approximate stats only (no overviews)")
   args = parser.parse_args()

   for num, image in enumerate(args.infiles):
      print("[{0}/{1}] {2}".format(num+1, len(args.infiles),
                                   os.path.basename(image)))

      image = get_gdal_drivers.GDALDrivers().zip_to_gdal_path(image)

      if args.approx_stats:
         calc_approx_stats(image)
      else:
         ds = gdal.Open(image, gdal.GA_Update)
         calcstats.calcStats(ds, ignore=args.ignore)
         ds = None
