ARG py_env_path=/env

# Add geobase wheels
FROM opendatacube/geobase:wheels as env_builder

ARG py_env_path
COPY requirements-docker.txt /
RUN /usr/local/bin/env-build-tool new /requirements-docker.txt ${py_env_path}
ENV PATH=${py_env_path}//bin:${PATH} \
    PYTHONPATH=${py_env_path}

WORKDIR /opt/

RUN git clone https://bitbucket.org/au-eoed/livingearth_lccs.git
RUN git clone https://bitbucket.org/geoscienceaustralia/livingearth_australia.git

RUN cd /opt/livingearth_australia/livingearth_australia && python3 setup.py install
RUN cd /opt/livingearth_lccs && python3 setup.py install


# Add geobase runner
FROM opendatacube/geobase:runner
ARG py_env_path
COPY --chown=1000:100 --from=env_builder $py_env_path $py_env_path
ENV PATH=${py_env_path}/bin:$PATH \
    PYTHONPATH=${py_env_path} \
    GDAL_DISABLE_READDIR_ON_OPEN="EMPTY_DIR" \
    CPL_VSIL_CURL_ALLOWED_EXTENSIONS=".tif, .tiff" \
    GDAL_HTTP_MAX_RETRY="10" \
    GDAL_HTTP_RETRY_DELAY="1"

# Ubuntu related unicode support
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN ls /opt
COPY --from=env_builder ./opt/livingearth_australia/deployment/*.py ./opt/livingearth_australia/deployment/*.txt /opt/deployment/
RUN mv /opt/deployment/process_lccs.py ./opt/
COPY --from=env_builder ./opt/livingearth_australia/deployment/helper /opt/helper
COPY --from=env_builder ./opt/livingearth_australia/livingearth_australia /opt/livingearth_australia
COPY --from=env_builder ./opt/livingearth_lccs/ /opt/livingearth_lccs/

ENV PYTHONPATH=${py_env_path}:'/opt/helper/':'/opt/livingearth_australia/le_plugins/'
ENV DB_HOSTNAME=postgres
ENV DB_USERNAME=opendatacube
ENV DB_PASSWORD=opendatacubepassword
ENV DB_DATABASE=opendatacube

# So AWS credentials aren't required when we access S3.
ENV AWS_NO_SIGN_REQUEST=Y
RUN pip install h5py==2.10.0

