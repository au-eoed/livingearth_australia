# Process LCCS based on tiles on SQS

import os
import sys
import boto3
import shutil
import yaml
from pathlib import Path
from livingearth_lccs.le_lccs.le_utils.gridded_classification import run_classification
import logging
import glob
from assume_role_helper import get_autorefresh_session

SQS_QUEUE = os.environ.get("SQS_QUEUE", 'dea-sandbox-eks-lccs')
S3_BUCKET = os.environ.get("S3_BUCKET", 'lccs-dev')
S3_PATH = os.environ.get("S3_PATH", "")
BASEPATH = os.environ.get("BASEPATH", "/tmp/lccs")
LCCS_CONFIG = os.environ.get("LCCS_CONFIG", '/opt/livingearth_australia/templates/l4_vp_template.yaml')
SESSION_DURATION = os.getenv('SESSION_DURATION', 43000)     # Default to ~11hours

VISIBILITYTIMEOUT = os.environ.get('VISIBILITYTIMEOUT', 3600)

# Set up boto3 connections for sqs and s3 resources
if 'AWS_ROLE_ARN' in os.environ and 'AWS_WEB_IDENTITY_TOKEN_FILE' in os.environ:
    role_with_web_identity_params = {
        "DurationSeconds": SESSION_DURATION,
        "RoleArn": os.getenv('AWS_ROLE_ARN'),
        "RoleSessionName": os.getenv('AWS_SESSION_NAME', 'test_session'),
        "WebIdentityToken": open(os.getenv('AWS_WEB_IDENTITY_TOKEN_FILE')).read(),
    }
    autorefresh_session = get_autorefresh_session(**role_with_web_identity_params)
    sqs = autorefresh_session.resource('sqs')
    s3r = autorefresh_session.resource('s3')
else:
    sqs = boto3.resource('sqs')
    s3r = boto3.resource('s3')

queue = sqs.get_queue_by_name(QueueName=SQS_QUEUE)

def load_config(conf):
    with open(conf, 'r') as f:
        return yaml.safe_load(f)

def get_tile_bounds(tile):
    x, y = tile.split(' ')
    minx = int(x) * 100000
    miny = int(y) * 100000
    maxx = minx + 100000
    maxy = miny + 100000

    return {'minx':minx,  'miny':miny, 'maxx':maxx, 'maxy':maxy}

def update_yaml(conf, tile, year, filename, outpath=""):
    # Tile of the format: {'minx':minx,  'miny':miny, 'maxx':maxx, 'maxy':maxy}

    conf['extent']['min_x'] = tile['minx']
    conf['extent']['min_y'] = tile['miny']
    conf['extent']['max_x'] = tile['maxx']
    conf['extent']['max_y'] = tile['maxy']

    conf['Outputs']['level3_data']['output_file'] = os.path.join(outpath, filename) + "L3_v-0.5.0.tif" # TODO add auto tagging for version here.
    conf['Outputs']['level3_rgb']['output_file'] = os.path.join(outpath, filename) + "L3_rgb_v-0.5.0.tif"

    conf['Outputs']['level4_data']['output_file'] = os.path.join(outpath, filename) + "L4_v-0.5.0.tif"
    conf['Outputs']['level4_rgb']['output_file'] = os.path.join(outpath, filename) + "L4_rgb_v-0.5.0.tif"


    # update start and end times
    start = f"{year}-01-01"
    end = f"{year}-12-31"

    # list of layers which need start & end dates
    l3_layers = ['vegetat_veg_cat', 'wofs_mask', 'item_v2_mask',
                 'mangrove', 'sklearn_cultivated_classification', 'tf_urban_classification', 'wcf_mask']

    for layer in l3_layers:
        conf['L3layers'][layer]['start_time'] = start
        conf['L3layers'][layer]['end_time'] = end

    l4_layers = ['lifeform_veg_cat', 'canopyco_veg_con', 'watersea_veg_cat', 'waterper_wat_cin', 'baregrad_phy_con']

    for layer in l4_layers:
        conf['L4layers'][layer]['start_time'] = start
        conf['L4layers'][layer]['end_time'] = end

    return conf

def save_yaml(filename, conf, outpath=""):
    with open(outpath + filename, 'w') as outfile:
        outfile.write(yaml.dump(conf))
        outfile.flush()
        outfile.close()

def upload_to_s3(path, files):
    logging.info("Commencing S3 upload")

    if S3_BUCKET:
        logging.info("Uploading to {}".format(S3_BUCKET))
        # Upload data
        for out_file in files:
            data = open(out_file, 'rb')
            key = "{}/{}/{}".format(S3_PATH, path, os.path.basename(out_file))
            logging.info("Uploading file {} to S3://{}/{}".format(out_file, S3_BUCKET, key))
            print("Uploading file {} to S3://{}/{}".format(out_file, S3_BUCKET, key))
            s3r.Bucket(S3_BUCKET).put_object(Key=key, Body=data)
            os.remove(out_file)
    else:
        logging.warning("Not uploading to S3, because the bucket isn't set.")


def count_messages():
    queue.load()
    return int(queue.attributes["ApproximateNumberOfMessages"])

if __name__ == "__main__":
    logging.info({"SQS": SQS_QUEUE, "Bucket": S3_BUCKET,
                  "Path": S3_PATH, "LCCS Config": LCCS_CONFIG})

    n_messages = count_messages()

    conf_template = load_config(LCCS_CONFIG)

    while n_messages > 0:
        messages = queue.receive_messages(
            VisibilityTimeout = VISIBILITYTIMEOUT,
            MaxNumberOfMessages = 1)

        if len(messages) > 0:
            message = messages[0]

            # break message into tile / year
            tile, year = message.body.split('/')
            print(f"Processing {tile} for {year}")
            bounds = get_tile_bounds(tile)

            out_filename = str(year) + '_' + tile.replace(' ', '_')


            if Path(BASEPATH).exists():
                shutil.rmtree(BASEPATH)
            os.makedirs(BASEPATH)

            # create updated yaml
            yaml_name = out_filename + ".yaml"
            new_yaml = update_yaml(conf_template, bounds, year, out_filename, BASEPATH)
            save_yaml(yaml_name, new_yaml)

            # run lccs code
            run_classification(config_file=yaml_name, year_of_product=year)

            files = glob.glob(f"{BASEPATH}/**/*.*", recursive=True)
            # upload to S3
            x, y = tile.split()
            path = f"{year}/x_{x}/y_{y}"
            upload_to_s3(path, files)

            os.remove(yaml_name)
            message.delete()
        else:
            print(" No messages found")

        n_messages = count_messages()
