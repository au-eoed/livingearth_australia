# LCCS AWS Orchestration

![awsorchestrationdiagram](awsdiagram.png "aws diagram")

Test using the manual `kubectl apply -f file.yaml` approach and raise a PR to the master branch of datakube-apps for large scale deployment using flux.

More documentation: [AWS Kuber/SQS process](https://docs.dev.dea.ga.gov.au/getting_started/workstation.html?highlight=kubernetes)

## Steps

### Access sandbox cluster
* AWS Vault access
  * `aws-vault add dea-sandbox-eks` - this only occurs once
  * `setup_aws_vault dea-sandbox-eks` - prompts for password
* Connect to cluster
  * `ap dea-sandbox-eks`
### SQS
* Queue name: `dea-sandbox-eks-lccs`
* SQS messages visible in [grafana](https://monitor.cloud.ga.gov.au/d/s86T1KDGz/lccs-monitoring?orgId=1)
* [Add items to queue](https://bitbucket.org/au-eoed/livingearth_australia/src/new_docker_setup/deployment/build-queue.py)
  * `python3 build-queue.py --year 2015 --tiles tilefile.txt`
* Get queue URL
  * `aws sqs get-queue-url --queue-name example`
* Display number of messages in queue
  * `aws sqs get-queue-attributes --queue-url https://example`
  * `aws sqs get-queue-attributes --queue-url https://example`
* Delete messages from queue
  * `aws sqs purge-queue --queue-url https://example`
### Deployment
* Clone [datakube-apps repo](https://bitbucket.org/geoscienceaustralia/datakube-apps/src/master/workspaces/dea-sandbox/processing/06_lccs_processing.yaml)
* Update `06_lccs_processing.yaml`
  * Insert desired Docker image name. Tags available via [dockerhub](https://hub.docker.com/r/geoscienceaustralia/lccs/tags?page=1&ordering=last_updated)
  * Update `S3_PATH` to match desired folder on S3
* Commands
  * `kubectl apply -f example.yaml`
  * `kubectl delete -f example.yaml`
  * `kubectl get pods -n processing`
  * `kubectl get pods -n processing | grep lccs`
  * `kubectl describe pod <pod-name> -n processing`
  * `kubectl logs pod-name -n processing -f`
* [Grafana pod output](https://mgmt.sandbox.dea.ga.gov.au/explore?orgId=1&left=%5B%22now-1h%22,%22now%22,%22Loki%22,%7B%22expr%22:%22%7Bapp%3D%5C%22lccs%5C%22%7D%22%7D,%7B%22mode%22:%22Logs%22%7D,%7B%22ui%22:%5Btrue,true,true,%22none%22%5D%7D%5D)
### Output
* Files will appear in [s3 bucket](https://s3.console.aws.amazon.com/s3/buckets/lccs-dev/)
* List 10 most recent uploads to S3
  * `aws s3 ls lccs-dev --recursive | sort | tail -n 10`
* List files within bucket folder
  * `aws s3 ls lccs-dev/folder --recursive --no-sign-request`
* Sync with local folder
  * `aws s3 --no-sign-request sync s3://lccs-dev/level1_vp ./folder/ --exclude "*" --include "*L4_rgb*"`
* Build vrts, tif and overview.
  * `gdalbuildvrt file.vrt 2010_*L4_rgb*.tif`
  * `gdal_translate -of GTiff -co "COMPRESS=LZW" -co "BIGTIFF=YES" -co "TILED=YES" \ file.vrt file.tif`
  * `gdaladdo file.tif 2 4 8 16`

### Notes
* Stale node solution. Problem only occurs occasionally.
  * ec2 -> autoscaling groups -> eks-***spot -> edit -> make desired capacity 1 then select update.
