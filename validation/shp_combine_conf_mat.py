import geopandas as gpd
import numpy as np
import pandas as pd
import sys
from pathlib import Path
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from pycm import *
import sys
import xarray as xr
sys.path.append("/g/data/u46/users/sc0554/dea-notebooks/Scripts")
from dea_spatialtools import xr_vectorize
pd.options.display.max_rows = 999

def plot_confusion_matrix(df_confusion, title='Confusion matrix', cmap=plt.cm.gray_r):
    plt.matshow(df_confusion, cmap=cmap) # imshow
    plt.colorbar()
    tick_marks = np.arange(len(df_confusion.columns))
    plt.xticks(tick_marks, df_confusion.columns, rotation=45)
    plt.yticks(tick_marks, df_confusion.index)
    plt.ylabel(df_confusion.index.name)
    plt.xlabel(df_confusion.columns.name)

target_names = ['111', '112', '124', '215', '216', '220']

# Folder containing clusters
#folder = Path(f"/g/data/r78/LCCS_Aberystwyth/validation/{year}/cleaned_data/")
folder = Path("cleaned_data/")
# Combine clusters into single geodataframe
gdf = pd.concat([
        gpd.read_file(shp)
            for shp in folder.glob("*.shp") 
            ], ignore_index=True).pipe(gpd.GeoDataFrame)

# Remove rows with mixed or unknown 
gdf = gdf[(gdf['output'] != 'm') & (gdf['output'] != 'u')]

gdf['valid'] = gdf['output'] == gdf['classified']

gdf.to_file('2010_2015_validation.shp')

confusion = classification_report(gdf['output'].to_numpy(), gdf['classified'].to_numpy(), target_names = target_names) 
print("2010 and 2015")
print(confusion)

gdf = pd.concat([
        gpd.read_file(shp)
            for shp in folder.glob("*2015*.shp") 
            ], ignore_index=True).pipe(gpd.GeoDataFrame)

# Remove rows with mixed or unknown 
gdf = gdf[(gdf['output'] != 'm') & (gdf['output'] != 'u')]

confusion = classification_report(gdf['output'].to_numpy(), gdf['classified'].to_numpy(), target_names = target_names) 

print("2015")
print(confusion)

gdf = pd.concat([
        gpd.read_file(shp)
            for shp in folder.glob("*2010*.shp") 
            ], ignore_index=True).pipe(gpd.GeoDataFrame)

# Remove rows with mixed or unknown 
gdf = gdf[(gdf['output'] != 'm') & (gdf['output'] != 'u')]

confusion = classification_report(gdf['output'].to_numpy(), gdf['classified'].to_numpy(), target_names = target_names) 

print("2010")
print(confusion)

# Write out merged shapefile

# Calculate confusion matrix

# Seperate points by another shapefile and generate plots and accuracy statistics
states = gpd.read_file("/g/data/r78/LCCS_Aberystwyth/layers/Australian_Coast/Australian_Coast.shp")
#koppen  = xr_vectorize(xr.open_rasterio("/g/data/r78/LCCS_Aberystwyth/layers/koppen.tif"))

#for region in np.unique(koppen):
#    print(region)
#    s = states[states['STATE']==state].to_crs('EPSG:3577')
#    states_clip = gpd.clip(gdf, s)
#    predicted = states_clip['classified']
#    actual = states_clip['output']
#    confusion = ConfusionMatrix(actual.to_numpy(), predicted.to_numpy())
#    confusion.save_csv(str(state))
#    confusion.plot(normalized=True, number_label=True)
#    plt.savefig(str(state))

for state in states['STATE']:
    if state != 'JBT':
        print(state)
        s = states[states['STATE']==state].to_crs('EPSG:3577')
        states_clip = gpd.clip(gdf, s)
        predicted = states_clip['classified'].to_numpy()
        actual = states_clip['output'].to_numpy()
        confusion = classification_report(actual, predicted, target_names = target_names)
        print(confusion)
        #confusion = ConfusionMatrix(actual, predicted)
        #confusion = pd.crosstab(actual, predicted)
        #confusion_norm = confusion / confusion.sum(axis=1)
        #plot_confusion_matrix(confusion_norm)
        #plt.savefig(str(state))
        #print(confusion)
