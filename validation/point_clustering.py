import geopandas as gpd
from sklearn.cluster import KMeans
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
gdf = gpd.read_file('stratified_points_validation.shp')

output = (
    gdf.assign(x=lambda df: df['geometry'].centroid.x)
       .assign(y=lambda df: df['geometry'].centroid.y)
)

coords = output[['x','y']].to_numpy()
kmeans = KMeans(n_clusters = 60).fit_predict(coords)
coords_cluster = np.column_stack((coords[:,0],coords[:,1], kmeans))
k=pd.DataFrame(coords_cluster, columns=['x','y','cluster'])
k = k.astype({"cluster": int})
output=output.merge(k, on=['x','y'])
output.drop(columns=['index','x','y'], inplace=True)
f, ax = plt.subplots(1, figsize=(20, 15))
ax.set_axis_off()
output.plot(column='cluster',cmap='Set2', ax=ax, markersize=80)
plt.savefig('cluster.png')

for i in np.unique(output['cluster']):
    print(f"writing file {i}")
    out = output[output['cluster'] == i]
    out.to_file(f"validation_{i}.shp")

#validators = output.sample(n=150, random_state = 1)

#validators.to_file('validation_comparison.shp')
