import xarray as xr
import geopandas as gpd
from datacube.utils.cog import write_cog
import numpy as np
import pandas as pd
import datacube
from affine import Affine
from datacube.utils.geometry import assign_crs
import sys

# Open file to stratify points across
input_file_path = "/g/data/r78/LCCS_Aberystwyth/continental/nov2020/2015_L3_clipped_v-1.0.0.tif"
#input_file_path = "/g/data/r78/LCCS_Aberystwyth/continental/nov2020/2010_L3_clipped_v-1.0.0.vrt"
da = xr.open_rasterio(input_file_path).squeeze(drop=True)
print(da)
print(da.values.shape)
# Hardcode class values from input data
classes = np.array([111, 112, 124, 215, 216, 220])

# Number of samples to create
n_sample = 6000

# Find the total number of pixels in each class
class_sizes =[]
for class_id in classes:
    class_sizes.append((da==class_id).sum().values)
class_sizes = np.array(class_sizes)

# total number of pixels
total_px = class_sizes.sum()
proportions = [((x / total_px) * 100) for x in class_sizes]
print("Total number of pixels:", total_px)
print("Classes:",classes)
print("class sizes:", class_sizes)
print("class proportions:", proportions)
sys.exit(0)
# These were calculated by adding 5% to the uncommon classes and subtracting 10% from the 2 most common classes.
frac_sample = [0.12, 0.48, 0.05, 0.05, 0.25, 0.05]
#frac_sample = [0.166, 0.166, 0.166, 0.166, 0.166, 0.166]
n_sample_class = (n_sample * np.array(frac_sample)).astype(int)
#n_sample_class = [3, 3, 3, 3, 3, 3]
class_dict = dict(zip(classes, class_sizes))
#sys.exit()
print("nsampleclass:", n_sample_class)
i = 0
label_picked = {}
for class_id, class_sizes in class_dict.items():
    n = n_sample_class[i]
    print("Sampling", n, "points from class", class_id)
    i = i + 1
    #print("Class proportion:", class_sizes/total_px)
    if class_sizes > 1e9:
        # slightly over sample
        n_sample_over = np.ceil(2*(n*len(da.x)*len(da.y)/class_sizes)).astype(int)
        random_x = np.random.choice(np.arange(len(da.x)), n_sample_over, replace=False)
        random_y = np.random.choice(np.arange(len(da.y)), n_sample_over, replace=False)
        match = da.values[random_y, random_x] == class_id
        random_y, random_x = random_y[match], random_x[match]
        if len(random_y) < n:
            print("Not enough points are picked, try increase the number of random points")
            break
        else:
            # Pick the actual number of desired samples
            pick = np.random.choice(np.arange(len(random_y)), n, replace=False)
            y, x = random_y[pick], random_x[pick]
    else:
        index = np.argwhere(da.values.flatten() == class_id).squeeze()
        picked = np.random.choice(index, n, replace=False)
        # convert back to x, y 
        y, x  = np.unravel_index(picked, da.values.shape)
    label_picked[class_id] = (y, x)

# Create dataframe of locations and respective classes    
for class_id in classes:
    y, x = label_picked[class_id]
    df = pd.DataFrame({'y': da.y[y].values, 'x':da.x[x].values})
    df['classified']=class_id.astype(str)
    df['output']=class_id.astype(str)
    df['processed']=0
    df['processed']=df['processed'].astype(str)
    if class_id == classes[0]: 
        dfs = df
    else: 
        dfs = dfs.append(df, ignore_index=True)

gdf_points = gpd.GeoDataFrame(
            dfs,
            crs=da.crs,
            geometry=gpd.points_from_xy(dfs.x, dfs.y)).reset_index()

gdf_points = gdf_points.drop_vars(['x', 'y'],axis=1)

# Write out file
gdf_points.to_file('/g/data/r78/LCCS_Aberystwyth/validation/training/training_points_2015.shp')
