#!/bin/bash
# This file is the bash pipeline for creating the Docker image for LCCS
# Clone relevant repos
#git clone --depth 1 --branch master https://bitbucket.org/au-eoed/livingearth_australia.git
git clone --depth 1 --branch master https://bitbucket.org/au-eoed/livingearth_lccs.git
git clone --depth 1 https://github.com/GeoscienceAustralia/dea-notebooks.git
# Move Scripts folder
mv dea-notebooks/Scripts .
# Create tag and use as image name
export TAG=$(git describe --tags)
export IMAGE_NAME=geoscienceaustralia/lccs:$TAG
#export IMAGE_LATEST=geoscienceaustralia/lccs:latest
# build the Docker image (this will use the Dockerfile in the root of the repo)
docker build -t $IMAGE_NAME .
#docker tag $IMAGE_NAME $IMAGE_LATEST  # also tag image with latest

# Optionally push image to docker hub.

while getopts ":p" opt; do
  case ${opt} in
    p )
      echo "Pushing to Docker hub" >&2
      docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD
      # push the new Docker image to the Docker registry
      docker push $IMAGE_NAME
#      docker push $IMAGE_LATEST
      ;;
  esac
done
